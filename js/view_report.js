var viz, workbook;
$(document).ready(function() {
	var view = getUrlParams()["viewurl"];
	var site = getUrlParams()["sitename"];
	$("#view_name").val(view);
	$("#site_name").val(site);
	getViewReportUrl(view,site);
	$("#subscribe_form").submit(function() {
		$.ajax({
			url : 'sendReport',
			type : 'post',
			data : $('#subscribe_form').serialize(),
			success : function(data) {
				if(data == "true"){
					alert("Report sent successfully.");
				}
			}
		});
		return false;
	});
})
function loadview(view_name) {
	var vizDiv = document.getElementById('viz');
	var vizURL = "http://192.168.10.92:8000/" + view_name;
	var options = {
		hideToolbar : true,
		hideTabs : true,

	}
	viz = new tableauSoftware.Viz(vizDiv, vizURL, options);
}
function getUrlParams() {

	var vars = [], hash;
	var hashes = window.location.href.slice(
			window.location.href.indexOf('?') + 1).split('&');
	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}

function getViewReportUrl(view,site) {
	

	$.ajax({
		type : "post",
		data : "view=" + view + "&site=" + site,
		url : "getViewReportUrl",
		success : function(msg) {
			if (msg.match("subscriptions.jsp")) {
				window.location = "subscriptions.jsp";
			} else {
				loadview(msg);

			}
		}
	})
}
