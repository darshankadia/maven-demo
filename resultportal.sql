-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2015 at 07:25 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `resultportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignsubject_mst`
--

CREATE TABLE IF NOT EXISTS `assignsubject_mst` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `faculty_name` varchar(40) NOT NULL,
  `subject_name` varchar(40) NOT NULL,
  `priamary` varchar(100) NOT NULL,
  PRIMARY KEY (`priamary`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch_mst`
--

CREATE TABLE IF NOT EXISTS `branch_mst` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(40) NOT NULL,
  `class` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `branch_name` (`branch_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `branch_mst`
--

INSERT INTO `branch_mst` (`id`, `branch_name`, `class`) VALUES
(42, 'COMPUTER', 'A,B,C,D,E'),
(44, 'ELECTRONICS COMMUNICATIONS', 'A'),
(46, 'MECHANICAL', 'A,B,C,D,E,F,G,H'),
(51, 'AERONAUTICAL', 'A'),
(52, 'CIVIL', 'A,B,C'),
(54, 'INFORMATION TECHNOLOGY', 'A,B');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_mst`
--

CREATE TABLE IF NOT EXISTS `faculty_mst` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `faculty_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `faculty_mst`
--

INSERT INTO `faculty_mst` (`id`, `faculty_name`) VALUES
(5, 'Darshan Kadia'),
(6, 'Dishant Shah'),
(7, 'Dwipal Kadia'),
(8, 'Nirav Thakkar');

-- --------------------------------------------------------

--
-- Table structure for table `login_mst`
--

CREATE TABLE IF NOT EXISTS `login_mst` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `coordinater_name` varchar(40) NOT NULL,
  `mob_no` varchar(10) NOT NULL,
  `email_id` varchar(40) NOT NULL,
  `branch` varchar(40) NOT NULL,
  `username` varchar(40) NOT NULL,
  `mid` varchar(10) NOT NULL DEFAULT 'Lock',
  `remid` varchar(10) NOT NULL DEFAULT 'Lock',
  `internal` varchar(10) NOT NULL DEFAULT 'Lock',
  `password` varchar(40) NOT NULL,
  `role` varchar(20) NOT NULL,
  `subject_name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `login_mst`
--

INSERT INTO `login_mst` (`id`, `coordinater_name`, `mob_no`, `email_id`, `branch`, `username`, `mid`, `remid`, `internal`, `password`, `role`, `subject_name`) VALUES
(4, 'Darshan Kadia', '9510353888', 'darshankadia.ce@gmail.com', 'Computer Engineering', 'darshan', 'Unlock', 'Unlock', 'Unlock', '123456', 'SuperAdmin', 'CALCULUS'),
(19, 'Disant Shah', '1234567891', 'abc@avc.com', 'Mechanical', 'dns25', 'Lock', 'Unlock', 'Lock', '123456', 'Admin', 'CS'),
(20, 'Dwipal Kadia', '1234567891', 'abc@avc.com', 'Computer', 'dwipal', 'Unlock', 'Unlock', 'Unlock', '123456', 'Admin', 'PHY'),
(21, 'Dixit Jain', '1234567891', 'abc@avc.com', 'Mechanical', 'dixit', 'Unlock', 'Unlock', 'Lock', '123456', 'Admin', 'CALCULUS'),
(22, 'Smit Patadiya', '1234567891', 'abc@avc.com', 'Mechanical', 'smit', 'Unlock', 'Unlock', 'Lock', '123456', 'Admin', 'CPU'),
(23, 'Ronak Dhruv', '1234567891', 'abc@avc.com', 'General', 'ronak', 'Unlock', 'Unlock', 'Lock', '123456', 'Admin', 'ECE'),
(24, 'Dhaval Mahajan', '1234567891', 'abc@avc.com', 'General', 'dhaval', 'Unlock', 'Unlock', 'Lock', '123456', 'Admin', 'W/S');

-- --------------------------------------------------------

--
-- Table structure for table `marks_mst`
--

CREATE TABLE IF NOT EXISTS `marks_mst` (
  `srno` int(10) NOT NULL AUTO_INCREMENT,
  `rollNo` varchar(20) NOT NULL,
  `branch` varchar(40) NOT NULL,
  `sem` int(2) NOT NULL,
  `class` varchar(2) NOT NULL,
  `student_name` varchar(40) NOT NULL,
  `subject_name` varchar(40) NOT NULL,
  `mid` varchar(4) NOT NULL DEFAULT '0',
  `remid` varchar(4) NOT NULL DEFAULT '0',
  `internal` varchar(4) NOT NULL DEFAULT '0',
  `midstatus` varchar(10) NOT NULL DEFAULT 'false',
  `remidstatus` varchar(10) NOT NULL DEFAULT 'false',
  `internalstatus` varchar(10) NOT NULL DEFAULT 'false',
  `prm` varchar(50) NOT NULL,
  PRIMARY KEY (`srno`),
  UNIQUE KEY `prm` (`prm`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=241 ;

--
-- Dumping data for table `marks_mst`
--

INSERT INTO `marks_mst` (`srno`, `rollNo`, `branch`, `sem`, `class`, `student_name`, `subject_name`, `mid`, `remid`, `internal`, `midstatus`, `remidstatus`, `internalstatus`, `prm`) VALUES
(240, 'ME-A-5', 'Mechanical', 1, 'A', 'Tona Poydras ', 'PHY', '16', '0', '0', 'true', 'true', 'false', 'ME-A-5PHY'),
(239, 'ME-A-4', 'Mechanical', 1, 'A', 'Magda Brazee ', 'PHY', '15', '16', '0', 'true', 'true', 'false', 'ME-A-4PHY'),
(238, 'ME-A-3', 'Mechanical', 1, 'A', 'Eda Delgadillo ', 'PHY', '12', '16', '0', 'true', 'true', 'false', 'ME-A-3PHY'),
(237, 'ME-A-2', 'Mechanical', 1, 'A', 'Mariela Tsang ', 'PHY', '30', '0', '0', 'true', 'true', 'false', 'ME-A-2PHY'),
(236, 'ME-A-1', 'Mechanical', 1, 'A', 'Cassie Quirion ', 'PHY', '40', '0', '0', 'true', 'true', 'false', 'ME-A-1PHY');

-- --------------------------------------------------------

--
-- Table structure for table `student_mst`
--

CREATE TABLE IF NOT EXISTS `student_mst` (
  `id` int(11) NOT NULL,
  `rollNo` varchar(20) NOT NULL,
  `class` varchar(10) NOT NULL,
  `branch` varchar(30) NOT NULL,
  `student_name` varchar(40) NOT NULL,
  `sem` int(1) NOT NULL,
  PRIMARY KEY (`rollNo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_mst`
--

INSERT INTO `student_mst` (`id`, `rollNo`, `class`, `branch`, `student_name`, `sem`) VALUES
(26, 'CE-A-1', 'A', 'Computer Engineering', 'Alissa Overly ', 1),
(27, 'CE-A-2', 'A', 'Computer Engineering', 'Tegan Kuzma ', 1),
(28, 'CE-A-3', 'A', 'Computer Engineering', 'Frederick Deibel ', 1),
(29, 'CE-A-4', 'A', 'Computer Engineering', 'Muoi Symons ', 1),
(30, 'CE-A-5', 'A', 'Computer Engineering', 'Lue Jalbert ', 1),
(31, 'CE-B-1', 'B', 'Computer Engineering', 'Kiesha Vogus ', 1),
(32, 'CE-B-2', 'B', 'Computer Engineering', 'Jacki Buchler ', 1),
(33, 'CE-B-3', 'B', 'Computer Engineering', 'Nida Marroquin ', 1),
(34, 'CE-B-4', 'B', 'Computer Engineering', 'Emmaline Story ', 1),
(35, 'CE-B-5', 'B', 'Computer Engineering', 'Lashawn Amezcua ', 1),
(16, 'CL-A-1', 'A', 'CIVIL', 'Edda Greenhaw ', 1),
(17, 'CL-A-2', 'A', 'CIVIL', 'Morton Towles ', 1),
(18, 'CL-A-3', 'A', 'CIVIL', 'Pura Broughton ', 1),
(19, 'CL-A-4', 'A', 'CIVIL', 'Tamesha Caldera ', 1),
(20, 'CL-A-5', 'A', 'CIVIL', 'Leanna Ange ', 1),
(21, 'CL-B-1', 'B', 'CIVIL', 'Morgan Scheff ', 1),
(22, 'CL-B-2', 'B', 'CIVIL', 'Marci Bordeau ', 1),
(23, 'CL-B-3', 'B', 'CIVIL', 'Susannah Ramirez ', 1),
(24, 'CL-B-4', 'B', 'CIVIL', 'Maryann Racey ', 1),
(25, 'CL-B-5', 'B', 'CIVIL', 'Classie Kephart ', 1),
(36, 'EC-A-1', 'A', 'ELECTRONICS AND COMMUNICATION', 'Loyce Neth ', 1),
(37, 'EC-A-2', 'A', 'ELECTRONICS AND COMMUNICATION', 'Olive Micheals ', 1),
(38, 'EC-A-3', 'A', 'ELECTRONICS AND COMMUNICATION', 'Jaye Koprowski ', 1),
(39, 'EC-A-4', 'A', 'ELECTRONICS AND COMMUNICATION', 'Melody Burwell ', 1),
(40, 'EC-A-5', 'A', 'ELECTRONICS AND COMMUNICATION', 'Alexandria Kyte ', 1),
(41, 'IT-A-1', 'A', 'INFORMATION TECHNOLOGY', 'Stevie Panton ', 1),
(42, 'IT-A-2', 'A', 'INFORMATION TECHNOLOGY', 'Kala Malpass ', 1),
(43, 'IT-A-3', 'A', 'INFORMATION TECHNOLOGY', 'Annie Kepley ', 1),
(44, 'IT-A-4', 'A', 'INFORMATION TECHNOLOGY', 'Erwin Fenimore ', 1),
(45, 'IT-A-5', 'A', 'INFORMATION TECHNOLOGY', 'Emil Brezinski ', 1),
(46, 'IT-B-1', 'B', 'INFORMATION TECHNOLOGY', 'Annett Sprinkle ', 1),
(47, 'IT-B-2', 'B', 'INFORMATION TECHNOLOGY', 'Lieselotte Sibrian ', 1),
(48, 'IT-B-3', 'B', 'INFORMATION TECHNOLOGY', 'Cristal Shaul ', 1),
(49, 'IT-B-4', 'B', 'INFORMATION TECHNOLOGY', 'Joye Linck ', 1),
(50, 'IT-B-5', 'B', 'INFORMATION TECHNOLOGY', 'Tori Barela ', 1),
(1, 'ME-A-1', 'A', 'Mechanical', 'Cassie Quirion ', 1),
(2, 'ME-A-2', 'A', 'Mechanical', 'Mariela Tsang ', 1),
(3, 'ME-A-3', 'A', 'Mechanical', 'Eda Delgadillo ', 1),
(4, 'ME-A-4', 'A', 'Mechanical', 'Magda Brazee ', 1),
(5, 'ME-A-5', 'A', 'Mechanical', 'Tona Poydras ', 1),
(6, 'ME-B-1', 'B', 'Mechanical', 'Shellie Voll ', 1),
(7, 'ME-B-2', 'B', 'Mechanical', 'Faustina Binion ', 1),
(8, 'ME-B-3', 'B', 'Mechanical', 'Jamar Mojica ', 1),
(9, 'ME-B-4', 'B', 'Mechanical', 'Carmon Ledgerwood ', 1),
(10, 'ME-B-5', 'B', 'Mechanical', 'Paulina Towner ', 1),
(11, 'ME-C-1', 'C', 'Mechanical', 'Arturo Wimmer ', 1),
(12, 'ME-C-2', 'C', 'Mechanical', 'Golda Brwon ', 1),
(13, 'ME-C-3', 'C', 'Mechanical', 'Zelda Catron ', 1),
(14, 'ME-C-4', 'C', 'Mechanical', 'Marvella Ewan ', 1),
(15, 'ME-C-5', 'C', 'Mechanical', 'Kristy Selley ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subject_mst`
--

CREATE TABLE IF NOT EXISTS `subject_mst` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(40) NOT NULL,
  `semister` int(2) NOT NULL,
  `subject_name` varchar(40) NOT NULL,
  `coordinater_name` varchar(40) NOT NULL,
  `class` varchar(1) NOT NULL,
  `prm` varchar(100) NOT NULL,
  `midmin` int(2) NOT NULL DEFAULT '16',
  `midmax` int(2) NOT NULL DEFAULT '40',
  `remidmin` int(2) NOT NULL DEFAULT '16',
  `remidmax` int(2) NOT NULL DEFAULT '40',
  `internalmin` int(2) NOT NULL DEFAULT '16',
  `internalmax` int(2) NOT NULL DEFAULT '40',
  PRIMARY KEY (`id`),
  UNIQUE KEY `prm` (`prm`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=121 ;

--
-- Dumping data for table `subject_mst`
--

INSERT INTO `subject_mst` (`id`, `branch_name`, `semister`, `subject_name`, `coordinater_name`, `class`, `prm`, `midmin`, `midmax`, `remidmin`, `remidmax`, `internalmin`, `internalmax`) VALUES
(1, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'A', 'MECHANICAL1CALCULUSA', 16, 40, 16, 40, 16, 40),
(2, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'A', 'MECHANICAL1CSA', 16, 40, 16, 40, 16, 40),
(3, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'A', 'MECHANICAL1PHYA', 16, 40, 16, 40, 16, 40),
(4, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'A', 'MECHANICAL1CPUA', 16, 40, 16, 40, 16, 40),
(5, 'MECHANICAL', 1, 'ECE', 'Ronak Dhruv', 'A', 'MECHANICAL1ECEA', 16, 40, 16, 40, 16, 40),
(6, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'A', 'MECHANICAL1W/SA', 16, 40, 16, 40, 16, 40),
(7, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'B', 'MECHANICAL1CALCULUSB', 16, 40, 16, 40, 16, 40),
(8, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'B', 'MECHANICAL1CSB', 16, 40, 16, 40, 16, 40),
(9, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'B', 'MECHANICAL1PHYB', 16, 40, 16, 40, 16, 40),
(10, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'B', 'MECHANICAL1CPUB', 16, 40, 16, 40, 16, 40),
(11, 'MECHANICAL', 1, 'ECE', 'Ronak Dhruv', 'B', 'MECHANICAL1ECEB', 16, 40, 16, 40, 16, 40),
(12, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'B', 'MECHANICAL1W/SB', 16, 40, 16, 40, 16, 40),
(13, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'C', 'MECHANICAL1CALCULUSC', 16, 40, 16, 40, 16, 40),
(14, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'C', 'MECHANICAL1CSC', 16, 40, 16, 40, 16, 40),
(15, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'C', 'MECHANICAL1PHYC', 16, 40, 16, 40, 16, 40),
(16, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'C', 'MECHANICAL1CPUC', 16, 40, 16, 40, 16, 40),
(17, 'MECHANICAL', 1, 'ECE', 'Ronak Dhruv', 'C', 'MECHANICAL1ECEC', 16, 40, 16, 40, 16, 40),
(18, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'C', 'MECHANICAL1W/SC', 16, 40, 16, 40, 16, 40),
(19, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'D', 'MECHANICAL1CALCULUSD', 16, 40, 16, 40, 16, 40),
(20, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'D', 'MECHANICAL1CSD', 16, 40, 16, 40, 16, 40),
(21, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'D', 'MECHANICAL1PHYD', 16, 40, 16, 40, 16, 40),
(22, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'D', 'MECHANICAL1CPUD', 16, 40, 16, 40, 16, 40),
(23, 'MECHANICAL', 1, 'ECE', 'Ronak Dhruv', 'D', 'MECHANICAL1ECED', 16, 40, 16, 40, 16, 40),
(24, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'D', 'MECHANICAL1W/SD', 16, 40, 16, 40, 16, 40),
(25, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'E', 'MECHANICAL1CALCULUSE', 16, 40, 16, 40, 16, 40),
(26, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'E', 'MECHANICAL1CSE', 16, 40, 16, 40, 16, 40),
(27, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'E', 'MECHANICAL1PHYE', 16, 40, 16, 40, 16, 40),
(28, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'E', 'MECHANICAL1CPUE', 16, 40, 16, 40, 16, 40),
(29, 'MECHANICAL', 1, 'ECE', 'Ronak Dhruv', 'E', 'MECHANICAL1ECEE', 16, 40, 16, 40, 16, 40),
(30, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'E', 'MECHANICAL1W/SE', 16, 40, 16, 40, 16, 40),
(31, 'AERONAUTICAL', 1, 'CALCULUS', 'Dixit Jain', 'A', 'AERONAUTICAL1CALCULUSA', 16, 40, 16, 40, 16, 40),
(32, 'AERONAUTICAL', 1, 'CS', 'Disant Shah', 'A', 'AERONAUTICAL1CSA', 16, 40, 16, 40, 16, 40),
(33, 'AERONAUTICAL', 1, 'PHY', 'Dwipal Kadia', 'A', 'AERONAUTICAL1PHYA', 16, 40, 16, 40, 16, 40),
(34, 'AERONAUTICAL', 1, 'CPU', 'Smit Patadiya', 'A', 'AERONAUTICAL1CPUA', 16, 40, 16, 40, 16, 40),
(35, 'AERONAUTICAL', 1, 'ECE', 'Ronak Dhruv', 'A', 'AERONAUTICAL1ECEA', 16, 40, 16, 40, 16, 40),
(36, 'AERONAUTICAL', 1, 'W/S', 'Dhaval Mahajan', 'A', 'AERONAUTICAL1W/SA', 16, 40, 16, 40, 16, 40),
(37, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'F', 'MECHANICAL1CALCULUSF', 16, 40, 16, 40, 16, 40),
(38, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'F', 'MECHANICAL1CSF', 16, 40, 16, 40, 16, 40),
(39, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'F', 'MECHANICAL1PHYF', 16, 40, 16, 40, 16, 40),
(40, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'F', 'MECHANICAL1CPUF', 16, 40, 16, 40, 16, 40),
(41, 'MECHANICAL', 1, 'EG', '', 'F', 'MECHANICAL1EGF', 16, 40, 16, 40, 16, 40),
(42, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'F', 'MECHANICAL1W/SF', 16, 40, 16, 40, 16, 40),
(43, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'G', 'MECHANICAL1CALCULUSG', 16, 40, 16, 40, 16, 40),
(44, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'G', 'MECHANICAL1CSG', 16, 40, 16, 40, 16, 40),
(45, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'G', 'MECHANICAL1PHYG', 16, 40, 16, 40, 16, 40),
(46, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'G', 'MECHANICAL1CPUG', 16, 40, 16, 40, 16, 40),
(47, 'MECHANICAL', 1, 'EG', '', 'G', 'MECHANICAL1EGG', 16, 40, 16, 40, 16, 40),
(48, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'G', 'MECHANICAL1W/SG', 16, 40, 16, 40, 16, 40),
(49, 'MECHANICAL', 1, 'CALCULUS', 'Dixit Jain', 'H', 'MECHANICAL1CALCULUSH', 16, 40, 16, 40, 16, 40),
(50, 'MECHANICAL', 1, 'CS', 'Disant Shah', 'H', 'MECHANICAL1CSH', 16, 40, 16, 40, 16, 40),
(51, 'MECHANICAL', 1, 'PHY', 'Dwipal Kadia', 'H', 'MECHANICAL1PHYH', 16, 40, 16, 40, 16, 40),
(52, 'MECHANICAL', 1, 'CPU', 'Smit Patadiya', 'H', 'MECHANICAL1CPUH', 16, 40, 16, 40, 16, 40),
(53, 'MECHANICAL', 1, 'EG', '', 'H', 'MECHANICAL1EGH', 16, 40, 16, 40, 16, 40),
(54, 'MECHANICAL', 1, 'W/S', 'Dhaval Mahajan', 'H', 'MECHANICAL1W/SH', 16, 40, 16, 40, 16, 40),
(55, 'ELECTRONICAL COMMUNICATIONS', 1, 'CALCULUS', 'Dixit Jain', 'A', 'ELECTRONICAL COMMUNICATIONS1CALCULUSA', 16, 40, 16, 40, 16, 40),
(56, 'ELECTRONICAL COMMUNICATIONS', 1, 'CS', 'Disant Shah', 'A', 'ELECTRONICAL COMMUNICATIONS1CSA', 16, 40, 16, 40, 16, 40),
(57, 'ELECTRONICAL COMMUNICATIONS', 1, 'EEE', '', 'A', 'ELECTRONICAL COMMUNICATIONS1EEEA', 16, 40, 16, 40, 16, 40),
(58, 'ELECTRONICAL COMMUNICATIONS', 1, 'EME', '', 'A', 'ELECTRONICAL COMMUNICATIONS1EMEA', 16, 40, 16, 40, 16, 40),
(59, 'ELECTRONICAL COMMUNICATIONS', 1, 'EG', '', 'A', 'ELECTRONICAL COMMUNICATIONS1EGA', 16, 40, 16, 40, 16, 40),
(60, 'ELECTRONICAL COMMUNICATIONS', 1, 'ES', '', 'A', 'ELECTRONICAL COMMUNICATIONS1ESA', 16, 40, 16, 40, 16, 40),
(61, 'INFORMATION TECHNOLOGY', 1, 'CALCULUS', 'Dixit Jain', 'A', 'INFORMATION TECHNOLOGY1CALCULUSA', 16, 40, 16, 40, 16, 40),
(62, 'INFORMATION TECHNOLOGY', 1, 'CS', 'Disant Shah', 'A', 'INFORMATION TECHNOLOGY1CSA', 16, 40, 16, 40, 16, 40),
(63, 'INFORMATION TECHNOLOGY', 1, 'EEE', '', 'A', 'INFORMATION TECHNOLOGY1EEEA', 16, 40, 16, 40, 16, 40),
(64, 'INFORMATION TECHNOLOGY', 1, 'EME', '', 'A', 'INFORMATION TECHNOLOGY1EMEA', 16, 40, 16, 40, 16, 40),
(65, 'INFORMATION TECHNOLOGY', 1, 'EG', '', 'A', 'INFORMATION TECHNOLOGY1EGA', 16, 40, 16, 40, 16, 40),
(66, 'INFORMATION TECHNOLOGY', 1, 'ES', '', 'A', 'INFORMATION TECHNOLOGY1ESA', 16, 40, 16, 40, 16, 40),
(67, 'INFORMATION TECHNOLOGY', 1, 'CALCULUS', 'Dixit Jain', 'B', 'INFORMATION TECHNOLOGY1CALCULUSB', 16, 40, 16, 40, 16, 40),
(68, 'INFORMATION TECHNOLOGY', 1, 'CS', 'Disant Shah', 'B', 'INFORMATION TECHNOLOGY1CSB', 16, 40, 16, 40, 16, 40),
(69, 'INFORMATION TECHNOLOGY', 1, 'EEE', '', 'B', 'INFORMATION TECHNOLOGY1EEEB', 16, 40, 16, 40, 16, 40),
(70, 'INFORMATION TECHNOLOGY', 1, 'EME', '', 'B', 'INFORMATION TECHNOLOGY1EMEB', 16, 40, 16, 40, 16, 40),
(71, 'INFORMATION TECHNOLOGY', 1, 'EG', '', 'B', 'INFORMATION TECHNOLOGY1EGB', 16, 40, 16, 40, 16, 40),
(72, 'INFORMATION TECHNOLOGY', 1, 'ES', '', 'B', 'INFORMATION TECHNOLOGY1ESB', 16, 40, 16, 40, 16, 40),
(73, 'COMPUTER', 1, 'CALCULUS', 'Dixit Jain', 'A', 'COMPUTER1CALCULUSA', 16, 40, 16, 40, 16, 40),
(74, 'COMPUTER', 1, 'CS', 'Disant Shah', 'A', 'COMPUTER1CSA', 16, 40, 16, 40, 16, 40),
(75, 'COMPUTER', 1, 'EEE', '', 'A', 'COMPUTER1EEEA', 16, 40, 16, 40, 16, 40),
(76, 'COMPUTER', 1, 'EME', '', 'A', 'COMPUTER1EMEA', 16, 40, 16, 40, 16, 40),
(77, 'COMPUTER', 1, 'EG', '', 'A', 'COMPUTER1EGA', 16, 40, 16, 40, 16, 40),
(78, 'COMPUTER', 1, 'ES', '', 'A', 'COMPUTER1ESA', 16, 40, 16, 40, 16, 40),
(79, 'COMPUTER', 1, 'CALCULUS', 'Dixit Jain', 'B', 'COMPUTER1CALCULUSB', 16, 40, 16, 40, 16, 40),
(80, 'COMPUTER', 1, 'CS', 'Disant Shah', 'B', 'COMPUTER1CSB', 16, 40, 16, 40, 16, 40),
(81, 'COMPUTER', 1, 'EEE', '', 'B', 'COMPUTER1EEEB', 16, 40, 16, 40, 16, 40),
(82, 'COMPUTER', 1, 'EME', '', 'B', 'COMPUTER1EMEB', 16, 40, 16, 40, 16, 40),
(83, 'COMPUTER', 1, 'EG', '', 'B', 'COMPUTER1EGB', 16, 40, 16, 40, 16, 40),
(84, 'COMPUTER', 1, 'ES', '', 'B', 'COMPUTER1ESB', 16, 40, 16, 40, 16, 40),
(85, 'COMPUTER', 1, 'CALCULUS', 'Dixit Jain', 'C', 'COMPUTER1CALCULUSC', 16, 40, 16, 40, 16, 40),
(86, 'COMPUTER', 1, 'CS', 'Disant Shah', 'C', 'COMPUTER1CSC', 16, 40, 16, 40, 16, 40),
(87, 'COMPUTER', 1, 'EEE', '', 'C', 'COMPUTER1EEEC', 16, 40, 16, 40, 16, 40),
(88, 'COMPUTER', 1, 'EME', '', 'C', 'COMPUTER1EMEC', 16, 40, 16, 40, 16, 40),
(89, 'COMPUTER', 1, 'EG', '', 'C', 'COMPUTER1EGC', 16, 40, 16, 40, 16, 40),
(90, 'COMPUTER', 1, 'ES', '', 'C', 'COMPUTER1ESC', 16, 40, 16, 40, 16, 40),
(91, 'COMPUTER', 1, 'CALCULUS', 'Dixit Jain', 'D', 'COMPUTER1CALCULUSD', 16, 40, 16, 40, 16, 40),
(92, 'COMPUTER', 1, 'CS', 'Disant Shah', 'D', 'COMPUTER1CSD', 16, 40, 16, 40, 16, 40),
(93, 'COMPUTER', 1, 'EEE', '', 'D', 'COMPUTER1EEED', 16, 40, 16, 40, 16, 40),
(94, 'COMPUTER', 1, 'EME', '', 'D', 'COMPUTER1EMED', 16, 40, 16, 40, 16, 40),
(95, 'COMPUTER', 1, 'EG', '', 'D', 'COMPUTER1EGD', 16, 40, 16, 40, 16, 40),
(96, 'COMPUTER', 1, 'ES', '', 'D', 'COMPUTER1ESD', 16, 40, 16, 40, 16, 40),
(97, 'COMPUTER', 1, 'CALCULUS', 'Dixit Jain', 'E', 'COMPUTER1CALCULUSE', 16, 40, 16, 40, 16, 40),
(98, 'COMPUTER', 1, 'CS', 'Disant Shah', 'E', 'COMPUTER1CSE', 16, 40, 16, 40, 16, 40),
(99, 'COMPUTER', 1, 'EEE', '', 'E', 'COMPUTER1EEEE', 16, 40, 16, 40, 16, 40),
(100, 'COMPUTER', 1, 'EME', '', 'E', 'COMPUTER1EMEE', 16, 40, 16, 40, 16, 40),
(101, 'COMPUTER', 1, 'EG', '', 'E', 'COMPUTER1EGE', 16, 40, 16, 40, 16, 40),
(102, 'COMPUTER', 1, 'ES', '', 'E', 'COMPUTER1ESE', 16, 40, 16, 40, 16, 40),
(103, 'CIVIL', 1, 'CALCULUS', 'Dixit Jain', 'A', 'CIVIL1CALCULUSA', 16, 40, 16, 40, 16, 40),
(104, 'CIVIL', 1, 'CS', 'Disant Shah', 'A', 'CIVIL1CSA', 16, 40, 16, 40, 16, 40),
(105, 'CIVIL', 1, 'EME', '', 'A', 'CIVIL1EMEA', 16, 40, 16, 40, 16, 40),
(106, 'CIVIL', 1, 'EEE', '', 'A', 'CIVIL1EEEA', 16, 40, 16, 40, 16, 40),
(107, 'CIVIL', 1, 'ECE', 'Ronak Dhruv', 'A', 'CIVIL1ECEA', 16, 40, 16, 40, 16, 40),
(108, 'CIVIL', 1, 'ES', '', 'A', 'CIVIL1ESA', 16, 40, 16, 40, 16, 40),
(109, 'CIVIL', 1, 'CALCULUS', 'Dixit Jain', 'B', 'CIVIL1CALCULUSB', 16, 40, 16, 40, 16, 40),
(110, 'CIVIL', 1, 'CS', 'Disant Shah', 'B', 'CIVIL1CSB', 16, 40, 16, 40, 16, 40),
(111, 'CIVIL', 1, 'EME', '', 'B', 'CIVIL1EMEB', 16, 40, 16, 40, 16, 40),
(112, 'CIVIL', 1, 'EEE', '', 'B', 'CIVIL1EEEB', 16, 40, 16, 40, 16, 40),
(113, 'CIVIL', 1, 'ECE', 'Ronak Dhruv', 'B', 'CIVIL1ECEB', 16, 40, 16, 40, 16, 40),
(114, 'CIVIL', 1, 'ES', '', 'B', 'CIVIL1ESB', 16, 40, 16, 40, 16, 40),
(115, 'CIVIL', 1, 'CALCULUS', 'Dixit Jain', 'C', 'CIVIL1CALCULUSC', 16, 40, 16, 40, 16, 40),
(116, 'CIVIL', 1, 'CS', 'Disant Shah', 'C', 'CIVIL1CSC', 16, 40, 16, 40, 16, 40),
(117, 'CIVIL', 1, 'PHY', 'Dwipal Kadia', 'C', 'CIVIL1PHYC', 16, 40, 16, 40, 16, 40),
(118, 'CIVIL', 1, 'CPU', 'Smit Patadiya', 'C', 'CIVIL1CPUC', 16, 40, 16, 40, 16, 40),
(119, 'CIVIL', 1, 'EG', '', 'C', 'CIVIL1EGC', 16, 40, 16, 40, 16, 40),
(120, 'CIVIL', 1, 'W/S', 'Dhaval Mahajan', 'C', 'CIVIL1W/SC', 16, 40, 16, 40, 16, 40);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
