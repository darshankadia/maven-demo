<?php

$role = "none";

if(isset($_SESSION['active']))
{
$username = $_SESSION['active'];
$role = $_SESSION['role'];
$faculty = $_SESSION['faculty'];


}


?>
<link rel="stylesheet" href="http://css-spinners.com/css/spinner/circles.css" type="text/css">

<?php  if($role == "Admin")  { ?>


<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/application.js"></script>
<script src="js/bootstrap-toggle.min.js"></script>
<script src="js/jQuery.print.js"></script>
<?php }else if ($role == "SuperAdmin")  {  ?>


<script src="../js/jquery-1.10.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/validator.min.js"></script>
<script src="../js/application.js"></script>
<script src="../js/bootstrap-toggle.min.js"></script>
<script src="../js/tableToExcel.js"></script>
<script src="../js/jquery.table2excel.js"></script>
<script src="../js/jQuery.print.js"></script>

<script type='text/javascript' src="../js/jspdf.debug.js"></script>





<?php  }?>

<script type="text/javascript">
	$(document).ready(function() {
		
		var active = window.location.pathname;

		
		if (active.indexOf("addBranch.php") > -1) {
			$(".addBranch").parent("li").addClass("active");
		}
		if (active.indexOf("addFaculty.php") > -1) {
			$(".addFaculty").parent("li").addClass("active");
		}
		if (active.indexOf("index.php") > -1) {
			$(".addMarks").parent("li").addClass("active");
		}
		if (active.indexOf("addSubject.php") > -1) {
			$(".addSubject").parent("li").addClass("active");
		}
		if (active.indexOf("assignFaculty.php") > -1) {
			$(".addSubject").parent("li").addClass("active");
		}
		if (active.indexOf("addStudent.php") > -1) {
			$(".addStudent").parent("li").addClass("active");
		}
		if (active.indexOf("viewResult.php") > -1) {
			$(".viewResult").parent("li").addClass("active");
		}
		if (active.indexOf("addMarks.php") > -1) {
			$(".addMarks").parent("li").addClass("active");
		}
		if (active.indexOf("index.php") > -1) {
			$(".addMarks").parent("li").addClass("active");
		}
		if (active.indexOf("") > -1) {
			$(".addMarks").parent("li").addClass("active");
		}
		
		$('[data-toggle="tooltip"]').tooltip();

	});
</script>




	<div class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a href="./" class="navbar-brand">SOCET</a>

			</div>


				
					<ul class="nav navbar-nav" id="list">
						<?php  if($role == "SuperAdmin")  { ?>
						<li><a href="addBranch.php" class="addBranch">Manage Branch </a> </li>
						<li><a href="addFaculty.php" class="addFaculty">Manage Co-ordinator </a> </li>
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle addSubject"	data-toggle="dropdown" role="button" aria-expanded="false"> 
								Manage Subject  <span class="caret"></span>
							</a>
							
							<ul class="dropdown-menu" role="menu">
									<li><a href="addSubject.php">Add Subject</a></li>
									<li><a href="assignFaculty.php">Assign Faculty</a></li>
							</ul>
						</li>
						
						
						<li><a href="viewResult.php" class="viewResult">View Result</a></li>
						<?php  }else if($role == "Admin") { ?>
						<li><a href="addMarks.php" class="addMarks">Add Marks  
						
						</a></li>
						<?php  } ?>
					</ul>
					
						<?php  if($role != "none")  { ?>
							<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle"	data-toggle="dropdown" role="button" aria-expanded="false"> 
								<?php echo "Welcome  , ".$faculty;  ?>
								<span class="caret"></span>
							</a>
							
							<ul class="dropdown-menu" role="menu">
						<!--		 <li><a href="editProfile.php"> <span class="glyphicon glyphicon-edit"></span>Edit Profile</a></li>   -->
								<li>
									<a href="<?php  if($role == "SuperAdmin") echo "../logout.php"; else echo "./logout.php";  ?>">
											<span class="glyphicon glyphicon-log-out"></span>
											Logout
									</a>
								</li>
							</ul>
						</li>

					</ul>
	<?php  } ?>
				
			


		</div>

	</div>