<?php 
ob_start();
session_start();

include 'inc123/Connection.php';

if(!isset($_SESSION['active']))
{
	header('Location: login.php');
}
$username = $_SESSION["active"];
$role = $_SESSION["role"];
$faculty = $_SESSION["faculty"];

if($role == 'SuperAdmin')
{
	header('Location: admin/index.php');
}


?>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Branch</title>

<link href="css/bootstrap.css" rel="stylesheet">




</head>

<!-- <body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload="">   -->
<body >

<?php include 'inc123/header.php';?>


<?php
if(!isset($_GET['opp']))
{
?>

	<div class="container" id="searchStudent">
	
		<div class="page-header">
			<h2 align="center">Add Marks</h2>
		</div>
		<div class="row centerForm">
			<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<div class="well">
						<form class="form-horizontal" method="post" data-toggle="validator"  role="form" >
								
						<fieldset>

<input type="hidden" value= "<?php echo $faculty; ?>" id="username">
						
						<div class="form-group"  id="branch">
								<label for="select" class="col-lg-3 control-label">Branch</label>
								<div class="col-lg-5">
									<select class="form-control" required="required" id="selectBranch" onChange="getSem();"
										 name="branchName">
									</select>
								</div>
							</div>

							
								<div class="form-group" style="display:none"  id="sem">
								<label for="select" class="col-lg-3 control-label">Semester</label>
								<div class="col-lg-5">
									<select class="form-control" required id="selsctSemester" onChange="showClass();"
										 name="Semester">
										 
									</select>
								</div>
							</div>
							
							<div class="form-group" style="display:none"  id="class">
								<label for="select" class="col-lg-3 control-label">Class</label>
								<div class="col-lg-5">
									<select class="form-control" required id="selectClass" onChange="showSubject();"
										 name="class">
										 
									</select>
								</div>
							</div>
							
							
						<div class="form-group" style="display:none" id="subject">
								<label for="select" class="col-lg-3 control-label">Subject</label>
								<div class="col-lg-5">
									<select class="form-control" onchange="showStudent()" required id="selectSubject" 
										 name="subject">
									</select>
								</div>
							</div>
							

			<div class='col-md-12'  style='display:none;' id='loader'>
							<div class='col-md-5'></div>
							<div class="circles-loader col-md-4"  align='center' >
								Loading…
							</div>
							<div class='col-md-3'></div>
							</div>


						</fieldset>					
				</div>
				<div class="col-lg-2"></div>
			</div>
		</div>
</form>

	</div>

	
	
	
	
	
	
	
	
	
	
	
	
	
	
<div class="container" id="fillStudentMarks" style="display:none;">
		<div class="page-header">
			<h1>Add Marks</h1>
		</div>
		<div class="panel panel-default form-horizontal col-md-12">
		<form class="form-horizontal" method="post" data-toggle="validator"   role="form"  >
				
					<div class="panel-body">
				<div class="form-group " >
								<div class="page-header" style="text-align:right;" >		
								
									<h4><b>For absent students put capital AB in marks input box.</b></h4>			
								</div>	
						
					<table class="table table-striped table-hover " >
						<thead>
							<tr>
								<th width="5%">Sr. No</th>
								<th width="10%">Roll No</th>
								<th width="18%">Student Name</th>
								<th width="10%">Branch </th>
								<th width="4%">Sem</th>
								<th width="8%">Subject</th>
								<th width="15%" style="text-align:center">Mid</th>
								<th width="15%" style="text-align:center">Re-Mid</th>
								<th width="15%" style="text-align:center">Internal</th>
							</tr>
						</thead>
						<tbody id="tableDataGroups">
						</tbody>
					</table>
					
				</div>
			</div>
			</form>
		</div>
	</div>
<?php

}
?>

	
	<script type="text/javascript">
		window.history.forward();
		function noBack() {
			window.history.forward();
		}
	</script>
	
	
	<script type="text/javascript">
	var users = null;
	var faculty = $("#username").val();
	var midmin = '';
	var midmax = '';
	var remidmin = '';
	var remidmax = '';
	var internalmin = '';
	var internalmax = '';
	

		
		function getBranch() {
					var str = "<option value='' >Select Branch</option>";
					
					$.ajax({
						type : "POST",
						data : "faculty=" + faculty,
						url : "manageMarks.php?getBranch=true",
						 beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
						success : function(msg) {
					
							var obj = $.parseJSON(msg);
								if(obj == "null")
								{
									$("#selectBranch").html(str);
								}else
								{
									$.each(obj, function(i) {
									
										index = obj[i]['index'];
										branch_name = obj[i]['branch_name'];
										
										str = str + "<option value='"+branch_name+"'>" + branch_name + "</option>";

											});
										$("#selectBranch").html(str);
								}
									$('#loader').hide();
						}
					})
				}

	function getSem(){
	
			if($('#selectBranch').val() != "")
			{
				
				
			var branch = $('#selectBranch').val();
			
				$.ajax({
						type : "POST",
						data : "branch=" + branch + "&faculty="+ faculty,
						url : "manageMarks.php?getSem=true",
						 beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
						success : function(msg) {
							var str = "<option value=''>Select Semester</option>";
							
							var obj = $.parseJSON(msg);
								if(obj == "null")
								{
									$("#selsctSemester").html(str);
								}else
								{
									$.each(obj, function(i) {
									
										id = obj[i]['id'];
										Semester = obj[i]['semister'];
										
										str = str + "<option value='"+Semester+"'>" + Semester + "</option>";

											});
										$("#selsctSemester").html(str);
								}
						$('#loader').hide();	
						}
					})
			
				$("#sem").show();
			}else
			{
				var str="";
				$("#tableDataGroups").html(str);
				$('#fillStudentMarks').hide();
				$("#sem").hide();
				$("#faculty").hide();	
			}
			$("#subject").hide();
			$("#class").hide();
	}
	
function showClass(){
			if($('#selsctSemester').val() != "")
			{
				var branch = $('#selectBranch').val();
				var sem = $('#selsctSemester').val();
			
						$.ajax({
						type : "POST",
						data : "branch=" + branch + "&sem=" + sem + "&faculty=" + faculty ,
						url : "manageMarks.php?getClass=true",
						 beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
						success : function(msg) {
						
							var str = "<option value=''>Select Subject</option>";
							var obj = $.parseJSON(msg);
							
								if(obj == "null")
								{
									str = ""								
									$("#selectClass").html(str);
								}else
								{
									$.each(obj, function(i) {
									
										id = obj[i]['id'];
										classs = obj[i]['class'];
										
										str = str + "<option value='"+classs+"'>" + classs + "</option>";

											});
										$("#selectClass").html(str);
								}
							$('#loader').hide();
						}
					})
				$('#class').show();
				
			}else{
							var str="";
				$("#tableDataGroups").html(str);
				$('#fillStudentMarks').hide();
				$('#class').hide();
				
				
			}
		$('#subject').hide();
		}
	
	function showSubject(){
			if($('#selectClass').val() != "")
			{
				var branch = $('#selectBranch').val();
				var sem = $('#selsctSemester').val();

						$.ajax({
						type : "POST",
						data : "branch=" + branch + "&sem=" + sem + "&faculty=" + faculty ,
						url : "manageMarks.php?getSubject=true",
						 beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
						success : function(msg) {
						
							var str = "<option value=''>Select Subject</option>";
							var obj = $.parseJSON(msg);
							
								if(obj == "null")
								{
									str = ""								
									$("#selectSubject").html(str);
								}else
								{
									$.each(obj, function(i) {
									
										id = obj[i]['id'];
										subject_name = obj[i]['subject_name'];
										
										str = str + "<option value='"+subject_name+"'>" + subject_name + "</option>";

											});
										$("#selectSubject").html(str);
								}
							$('#loader').hide();
						}
					})
				$('#subject').show();
				
			}else{
							var str="";
				$("#tableDataGroups").html(str);
				$('#fillStudentMarks').hide();
				$('#subject').hide();
			}
		
		}
		
		
		function showStudent(){
			
			var str = "";
			if($('#selectSubject').val() != "")
			{
				
			getValidation();	
				var branch = $('#selectBranch').val();
				var sem = $('#selsctSemester').val();
				var classs = $('#selectClass').val();
				var subject = $('#selectSubject').val();
				
				

						$.ajax({
						type : "POST",
						data : "branch=" + branch + "&sem=" + sem + "&faculty=" + faculty  + "&subject=" + subject +"&class="+classs ,
						url : "manageMarks.php?getStudent=true",
						 beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
						success : function(msg) {
						
					var obj = "";
					
					var j = 1;
					obj = $.parseJSON(msg);
				
					
					
					if(obj == "null")
					{
					
						$("#tableDataGroups").html(str);
					}else{
						
						$.each(obj, function(i) {

							srno = obj[i]['srno'];
							rollNo = obj[i]['rollNo'];
							name = obj[i]['student_name'];
							branch = obj[i]['branch'];
							sem = obj[i]['sem'];
							mid = obj[i]['mid'];
							remid = obj[i]['remid'];
							internal = obj[i]['internal'];
							
							if(mid == 'Lock')
							{
								mid = "disabled";
							}else{
								mid = "";
							}
							if(remid == 'Lock')
							{
								remid = "disabled";
							}else{
								remid = "";
							}
							if(internal == 'Lock')
							{
								internal = "disabled";
							}else{
								internal = "";
							}
						
						
							var user = "";
							
							
							var checkMidMarks = 'checkMidMarks(this.value , this.id , event);';
							var checkReMidMarks = 'checkReMidMarks(this.value , this.id , event);';
							var checkInternalMarks = 'checkInternalMarks(this.value , this.id , event);';
							var checkZero = 'checkZero(this.value , this.id , event);';
							
							str = str
							+ "<tr>"
							+"<td>" 
							+ j
							+ "</td>"
							+ "<td>"
							+"<label data-toggle='tooltip' data-placement='right' data-original-title='" + rollNo + "'>" + rollNo + "</label>"
							+ "</td>"
							+ "<td>"
							+"<label id='"+name+"' data-toggle='tooltip' data-placement='right' data-original-title='" + name + "'>" + name + "</label>"
							+ "</td>"
							+ "<td>"
							+"<label id='"+branch+"' data-toggle='tooltip' data-placement='right' data-original-title='" + branch + "'>" + branch + "</label>"
							+ "</td>"
							+ "<td>"
							+"<label id='"+sem+"' data-toggle='tooltip' data-placement='right' data-original-title='" + sem + "'>" + sem + "</label>"
							+ "</td>"
							+ "<td>"
							+"<label id='"+subject+"' data-toggle='tooltip' data-placement='right' data-original-title='" + subject + "'>" + subject + "</label>"
							+ "</td>"
							+ "<td > "
							+"<input type='text'  onkeyup='"+checkMidMarks+"' onblur='"+checkZero+"' maxlength='2' id='mid"+rollNo+"' "+ mid +"  class='btn btn-danger pull-right  col-sm-7' value='0' name = 'mid"+rollNo+"' />"
							+"<input type='hidden'   value='"+rollNo+"' name='"+j+"rollNo'/>"
							+"<input type='hidden'   value='"+name+"' name='"+j+"name'/>"
							+ "</td>"
							+ "<td > "
							+"<input type='text'  onkeyup='"+checkReMidMarks+"'  onblur='"+checkZero+"' maxlength='2'  id='remid"+rollNo+"' "+ remid +" class='btn btn-danger pull-right col-sm-7' value='0' name = 'remid"+rollNo+"' />"
							+ "</td>"
							+ "<td > "
							+"<input type='text'  onkeyup='"+checkInternalMarks+"' onblur='"+checkZero+"' maxlength='2' id='internal"+rollNo+"'   "+ internal +" class='btn btn-danger pull-right col-sm-7' value='0' name = 'internal"+rollNo+"' />"
							+ "</td>"
							+ "</tr>"
							
					j++;
						});
						j = j-1;
						str = str 
							+ "<tr>"
							+"<td colspan='6' align='center'>" 
							+"<input type='hidden'   value='"+j+"' name='count'/>"
							+"<input type='hidden'   value='"+branch+"' name='branch'/>"
							+"<input type='hidden'   value='"+sem+"' name='sem'/>"
							+"<input type='hidden'   value='"+classs+"' name='class'/>"							
							+"<input type='hidden'   value='"+subject+"' name='subject'/>"
							+"<input type='button'  onclick='search()' class='btn btn-warning pull-center' value='Back To Search' name='addMarks'/>"
							+"</td>"
							+"<td>"
							+"<input type='button' class='btn btn-success pull-right' "+mid+" value='Add Marks' onclick='midadd();' id='addMidMarks' name='addMidMarks'/> "
							+"</td>"
							+"<td>"
							+"<input type='button' class='btn btn-success pull-right' "+remid+"  value='Add Marks' onclick='remidadd();'  id='addReMidMarks' name='addReMidMarks'/> "
							+"</td>"
							+"<td>"
							+"<input type='button' class='btn btn-success pull-right'  "+internal+"  value='Add Marks' onclick='internaladd();' id='addInternalMarks' name='addInternalMarks'/> "
							+ "</td>"
							+ "</tr>"
						
							$("#tableDataGroups").html(str);
					}
					
							$('[data-toggle="tooltip"]').tooltip();
							$('#fillStudentMarks').show();
							$('#searchStudent').hide();
					
							searchMarks(branch , sem,subject , classs);
							$('#loader').hide();
						}
					})
				
				
			}else{
				
				$("#tableDataGroups").html(str);
				$('#fillStudentMarks').hide();
			
			}
		
		}
		
		function getValidation()
		{
				var branch = $('#selectBranch').val();
				var sem = $('#selsctSemester').val();
				var classs = $('#selectClass').val();
				var subject = $('#selectSubject').val();
			
			var formData = $('form').serialize();	
				

						$.ajax({
						type : "POST",
						data : "branchName=" + branch + "&Semester=" + sem +  "&subject=" + subject +"&class="+classs ,
						url : "manageMarks.php?getValidation=true",
						 beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
						success : function(msg) {

							var obj = $.parseJSON(msg);

							if(obj != "null")
							{

								$.each(obj, function(i) {
								
										midmin = obj[i]['midmin'];
										midmax = obj[i]['midmax'];
										remidmin = obj[i]['remidmin'];
										remidmax = obj[i]['remidmax'];
										internalmin = obj[i]['internalmin'];
										internalmax = obj[i]['internalmax'];
												
							});
								
							}
							
							$('#loader').hide();
						}
						
						})
		}
		/*	$('#addmidMarks').submit(function(e) {
				// You can get the form instance

				alert('abc');
				})
			*/	
	
				
	function midadd()
	{
		var formData = $('form').serialize();
		
		
	
		$.ajax({
		
			type : "POST",
			data:  formData,
			url: "manageMarks.php?addmid=true",
				 beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
			success : function(msg) {
			
					$('#addMidMarks').val('Update Marks');
					alert('Midsem marks has been uploaded Sucessfully');
					$('#loader').hide();
				}
		})
	}	
	function remidadd()
	{
		var formData = $('form').serialize();
		
		
	
		$.ajax({
		
			type : "POST",
			data:  formData,
			url: "manageMarks.php?addremid=true",
			beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
			success : function(msg) {
					
					$('#addReMidMarks').val('Update Marks');
					alert('Remedial exam marks has been uploaded Sucessfully');
			$('#loader').hide();
				}
		})
	}	
	
	function internaladd()
	{
		var formData = $('form').serialize();
		
		
	
		$.ajax({
		
			type : "POST",
			data:  formData,
			url: "manageMarks.php?addinternal=true",
			beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
			success : function(msg) {
				alert(msg);
					$('#addInternalMarks').val('Update Marks');
					alert('Internal exam marks has been uploaded Sucessfully');
					$('#loader').hide();
			
				}
		})
	}	
				
		
	function search()
	{
		$('#fillStudentMarks').hide();
		$("#selectSubject").val('');
		$('#searchStudent').show();
		
	}

	function checkMidMarks(marks , id , e)
	{
		if(marks >= midmin)
		{
			$( "#"+id ).addClass("btn-success");
			$( "#"+id ).removeClass("btn-danger");
		}
		else{
			$( "#"+id ).addClass("btn-danger");
			$( "#"+id ).removeClass("btn-success");			
			}
			
		
		if(marks.length == '2')
		{
			if(marks.charCodeAt('0') == 65 )
			{
				if( marks.charCodeAt('1') != 66)
				{
					var mar = marks.replace(/[^]+/i, marks.charAt(0)+'');
					$( "#"+id ).val(mar);
				}
			}else
			{
				var mar = marks.replace(/[^0-9]+/i, '');
				$( "#"+id ).val(mar);
			}
		
		}else
		{
			if(marks.charCodeAt('0') != 65 )
			{
				var mar = marks.replace(/[^0-9]+/i, '');
				$( "#"+id ).val(mar);
			}	
		}			
			
			if(marks > midmax)
			{
				alert('Please enter marks between 0 to ' + midmax);
				$( "#"+id ).val('0');
			}
	            
	}
function checkReMidMarks(marks , id , e)
	{
		
		if(marks >= remidmin)
		{
			
			$( "#"+id ).addClass("btn-success");
			$( "#"+id ).removeClass("btn-danger");
		}
		else{
			$( "#"+id ).addClass("btn-danger");
			$( "#"+id ).removeClass("btn-success");			
			}
			
		
		if(marks.length == '2')
		{
			if(marks.charCodeAt('0') == 65 )
			{
				if( marks.charCodeAt('1') != 66)
				{
					var mar = marks.replace(/[^]+/i, marks.charAt(0)+'');
					$( "#"+id ).val(mar);
				}
			}else
			{
				var mar = marks.replace(/[^0-9]+/i, '');
				$( "#"+id ).val(mar);
			}
		
		}else
		{
			if(marks.charCodeAt('0') != 65 )
			{
				var mar = marks.replace(/[^0-9]+/i, '');
				$( "#"+id ).val(mar);
			}	
		}			
			
			if(marks > remidmax)
			{
				alert('Please enter marks between 0 to ' + remidmax );
				$( "#"+id ).val('0');
			}
	            
	}
function checkInternalMarks(marks , id , e)
	{
		if(marks >= internalmin)
		{
			$( "#"+id ).addClass("btn-success");
			$( "#"+id ).removeClass("btn-danger");
		}
		else{
			$( "#"+id ).addClass("btn-danger");
			$( "#"+id ).removeClass("btn-success");			
			}
			
		
		if(marks.length == '2')
		{
			if(marks.charCodeAt('0') == 65 )
			{
				if( marks.charCodeAt('1') != 66)
				{
					var mar = marks.replace(/[^]+/i, marks.charAt(0)+'');
					$( "#"+id ).val(mar);
				}
			}else
			{
				var mar = marks.replace(/[^0-9]+/i, '');
				$( "#"+id ).val(mar);
			}
		
		}else
		{
			if(marks.charCodeAt('0') != 65 )
			{
				var mar = marks.replace(/[^0-9]+/i, '');
				$( "#"+id ).val(mar);
			}	
		}			
			
			if(marks > internalmax)
			{
				alert('Please enter marks between 0 to ' + internalmax );
				$( "#"+id ).val('0');
			}
	            
	}	
	function checkZero(marks , id , e)
	{
	

				if(marks == "")
			{
				$( "#"+id ).val('0');
			}
	}
	
	function searchMarks(branch ,sem , subject , classs)
	{
	

		
		$.ajax({
		
			type : "POST",
			data : "branch=" + branch + "&sem=" + sem + "&faculty=" + faculty  + "&subject=" + subject +"&class="+classs ,
			url : "manageMarks.php?getStatus=true",
			beforeSend: function() {
							// setting a timeout
								$('#loader').show();
					},
			success : function(msg) {

							var obj = $.parseJSON(msg);

							if(obj != "null")
							{

								$.each(obj, function(i) {
								
													id = obj[i]['id'];
													mid = obj[i]['mid'];
													remid = obj[i]['remid'];
													internal = obj[i]['internal'];
													rollNo = obj[i]['rollNo'];
													midstatus = obj[i]['midstatus'];
													remidstatus = obj[i]['remidstatus'];
													internalstatus = obj[i]['internalstatus'];
												
												$( "#mid"+rollNo ).val(mid);
												
												if(mid >= midmin){
													$("#remid"+rollNo).prop('disabled', true);
												$( "#remid"+rollNo ).val('NA');
												}else {
												$( "#remid"+rollNo ).val(remid);
												}
												$( "#internal"+rollNo ).val(internal);
												
												
												
												
														if(mid >= midmin)
														{
															$( "#mid"+rollNo ).addClass("btn-success");
															$( "#mid"+rollNo ).removeClass("btn-danger");		
														}else{
															$( "#mid"+rollNo ).addClass("btn-danger");
															$( "#mid"+rollNo ).removeClass("btn-success");																	
														}
														
														if(remid >= remidmin)
														{
															$( "#remid"+rollNo ).addClass("btn-success");
															$( "#remid"+rollNo ).removeClass("btn-danger");		
														}else{
															$( "#remid"+rollNo ).addClass("btn-danger");
															$( "#remid"+rollNo ).removeClass("btn-success");																	
														}
														
														if(internal >= internalmin)
														{
															$( "#internal"+rollNo ).addClass("btn-success");
															$( "#internal"+rollNo ).removeClass("btn-danger");		
														}else{
															$( "#internal"+rollNo ).addClass("btn-danger");
															$( "#internal"+rollNo ).removeClass("btn-success");																	
														}
									});
									if(midstatus == 'true')
												{
												
													$('#addMidMarks').val('Update Marks');
												}
												if(remidstatus == 'true')
												{
													$('#addReMidMarks').val('Update Marks');
												}
												if(internalstatus == 'true')
												{
													$('#addInternalMarks').val('Update Marks');
												}
								

								
								
								
							}
							
							$('#loader').hide();
						}
					})
	}
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			getBranch();
			
		
			

			
		})
		
		
	
		
	</script>

</body>
<?php include 'inc123/footer.html'; ?>		
</html>

