<?php 
ob_start();
session_start();

include 'inc123/Connection.php';

if(isset($_SESSION['active']))
{
	$username = $_SESSION["active"];
	$role = $_SESSION["role"];
			
	if($role == 'Admin')
	{	
		header('Location: index.php');
	}else 
	{
		header('Location: admin/index.php');
	}		

}


if(isset($_POST['submit']))
{
$username = $_POST['username'];
$password = $_POST['password'];


$query = "select * from login_mst where username = '$username' and password = '$password'";
$sucess = mysqli_query($connection,$query);
	
$count = mysqli_num_rows($sucess);


		if($count == 1)
			{
					
					while(@$row=mysqli_fetch_assoc($sucess)) 
						{
							$role = $row['role'];
							$faculty = $row['coordinater_name'];
						}	
			
					$_SESSION['active'] = $username;
					$_SESSION['role'] = $role;
					$_SESSION['faculty'] = $faculty;
					
					if($role == "Admin")
					{
						header('Location: index.php');
						
					}else if($role == "SuperAdmin")
					{
						echo "check";
						
						header('Location: admin/index.php');
					}
			}
		else
			{
				echo"<script>alert('User Name or Password Incorrect');</script>";
			}

}

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>

<link href="css/bootstrap.css" rel="stylesheet">


</head>

<body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload="">


<?php include 'inc123/header.php';?>

	<div class="container">
		
		<div class="alert alert-warning" id="error" style="display: none">
			<strong>Warning ! </strong>
		</div>

		<div class="alert alert-success" id="success" style="display: none">
			<strong>Sucess ! </strong>
		</div>

		<div class="page-header">
			<h2 align="center">Login</h2>
		</div>
		
		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-6">
				<div class="well">
					<form class="form-horizontal" data-toggle="validator" role="form"
						action="login.php" id="my_form" method="post">
						<input type="hidden" name="login" value="true" />
						<fieldset>

							<div class="form-group">
								<label for="inputUsername" class="control-label col-lg-2">Username</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="username"
										id="inputUsername" placeholder="Enter Username"
										data-error="Enter Username" required> <span
										class="help-block with-errors"></span>
								</div>
							</div>

							<div class="form-group">
								<label for="inputPassword" class="control-label col-xs-2">Password</label>
								<div class="col-lg-10">
									<input type="password" class="form-control" name="password"
										id="inputPassword" placeholder="Enter Password"
										data-error="Enter Password" required>
									<div class="help-block with-errors"></div>
								</div>
							</div>

							<div class="form-group col-lg-12">
							<div class="col-lg-3"></div>
								<div class="col-xs-offset-2 col-xs-10 col-lg-4">
									<button type="submit" name="submit" class="btn btn-info">Login</button>
									
								</div>
						<div class="col-lg-5"></div>
							</div>

						</fieldset>
					</form>
				</div>
			</div>
			<div class="col-lg-3"></div>
		</div>
	</div>


	
	<script type="text/javascript">
		window.history.forward();
		function noBack() {
			window.history.forward();
		}
	</script>
</body>

<?php include 'inc123/footer.html'; ?>	
</html>

